import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField wins;
    private TextField ties;
    private TextField losses;
    private RpsGame game;
    private String choice;

    public RpsChoice(TextField message, TextField wins, TextField ties, TextField losses, String choice, RpsGame game){
        this.message = message;
        this.wins = wins;
        this.ties = ties;
        this.losses = losses;
        this.game = game;
        this.choice = choice;
    }

    @Override
    public void handle(ActionEvent e) {
        String result = this.game.playRound(this.choice);
        this.message.setText(result);

        int updatedWins = this.game.getWins();
        this.wins.setText("wins: " + updatedWins);

        int updatedTies = this.game.getTies();
        this.ties.setText("ties: " + updatedTies);

        int updatedLosses = this.game.getLosses();
        this.losses.setText("losses: " + updatedLosses);
        System.out.println(message);
    }
    
}
