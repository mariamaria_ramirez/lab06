import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsAppliation extends Application{
	private RpsGame game = new RpsGame();

    public void start(Stage stage) {
		
        Group root = new Group();
		VBox display = new VBox();
		HBox buttons = new HBox(); 

		Button rock = new Button("rock");
		Button paper = new Button("paper");
		Button sissors = new Button("sissors");
		buttons.getChildren().addAll(rock, paper, sissors);
		

		HBox textFields = new HBox();

		TextField welcome = new TextField("Welcome!");
		TextField wins = new TextField("Wins: 0");
		TextField ties = new TextField("Ties: 0");
		TextField losses = new TextField("losses: 0");

		RpsChoice choice1 = new RpsChoice(welcome, wins, ties, losses, rock.getText(), this.game);
		rock.setOnAction(choice1);
		RpsChoice choice2 = new RpsChoice(welcome, wins, ties, losses, sissors.getText(), this.game);
		sissors.setOnAction(choice2);
		RpsChoice choice3 = new RpsChoice(welcome, wins, ties, losses, paper.getText(), this.game);
		paper.setOnAction(choice3);

		textFields.getChildren().addAll(welcome, wins, ties, losses);

		display.getChildren().addAll(buttons, textFields);

		root.getChildren().add(display);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    