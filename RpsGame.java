import java.util.Random;

public class RpsGame{
    private int wins = 0;
    private int ties = 0;
    private int losses = 0;
    //private String choice = "";
    private Random rand;

    public int getWins(){
        return this.wins;
    }

    public int getTies(){
        return this.ties;
    }

    public int getLosses(){
        return this.losses;
    }

    public String playRound(String choice){
        String[] symbols = {"rock", "sissors", "paper"};
        this.rand = new Random();
        int number = rand.nextInt(3);
        String compChoice = symbols[number];

        boolean case1 = compChoice.equals(symbols[0]) && choice.equals(symbols[1]);
        boolean case2 = compChoice.equals(symbols[1]) && choice.equals(symbols[2]);
        boolean case3 = compChoice.equals(symbols[2]) && choice.equals(symbols[0]);

        if(case1 || case2 || case3){
            System.out.println("looser");
            this.losses++;
        }
        else if(choice.equals(compChoice)){
            System.out.println("tie!");
            this.ties++;
        }
        else if(case1 == false && case2 == false && case3 == false){
            System.out.println("winner!");
            this.wins++;
        }

        return "computer chose " + compChoice ;
    }
}