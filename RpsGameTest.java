import java.util.Scanner;

public class RpsGameTest {
    public static void main (String[] args){
        Scanner reader = new Scanner(System.in);
        System.out.println("choose rock, paper, or sissors");
        String choice = reader.nextLine();
        reader.close();

        RpsGame game = new RpsGame();
        String results = game.playRound(choice);
        System.out.println(results);
    }
}
